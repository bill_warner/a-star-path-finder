from matplotlib import colors
import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk

class Node(object):
    def __init__(self, parent_node=None, position=None):
        self.parent_node = parent_node
        self.position = position
        self.f = 0
        self.g = 0
        self.h = 0

    def __eq__(self, other):
        return self.position == other.position

class AStar(tk.Tk,Node):
    def __init__(self):
        super().__init__()
        self.height = 600
        self.width = 600
        self.grid_line_count = 25
        self.grid_increment = int(self.width/self.grid_line_count)
        self.canvas = tk.Canvas(self, width=self.width, height=self.height)
        self.canvas.pack()
        self.cells = {}
        self.submitted = tk.IntVar()
        self.maze = None
        self.x_path = None
        self.y_path = None
        self.start_button_pressed = False
        self.end_button_pressed = False
        self.start = None
        self.end = None
        self.cell_clicked = False

    def exit_canvas(self):
        self.canvas.destroy()

    def submit(self):
        maze_list = []
        self.submitted.set(1)
        for cell in self.cells:
            if self.cells[cell]['filled']:
                wall = 1
            else:
                wall = 0
            maze_list.append(wall)
            cell_coords = tuple(int(i/self.grid_increment) for i in cell)
            if self.cells[cell]['start']:
                self.start = cell_coords
            if self.cells[cell]['end']:
                self.end = cell_coords

        self.maze = [maze_list[i:i+self.grid_line_count] for i in range(0, len(maze_list), self.grid_line_count)]

    def select_start(self):
        self.start_button_pressed = True

    def select_end(self):
        self.end_button_pressed = True

    def set_color(self, cell, new_color, old_color):
        cell_color = self.canvas.itemcget(cell, "fill")
        new_color = new_color if  cell_color == old_color else old_color
        self.canvas.itemconfigure(cell, fill=new_color)

    def set_cell(self, x, y, cell_type):
        self.cells[x,y][cell_type] = True
        cell = self.cells[x,y]['cell']
        if cell_type == 'start':
            self.start_button_pressed = False
            self.set_color(cell, 'green','white')
        elif cell_type == 'end':
            self.end_button_pressed = False
            self.set_color(cell, 'red','white')

    def cell_formator(self, x, y):
        if self.start_button_pressed:
            self.set_cell(x,y,'start')
        elif self.end_button_pressed:
            self.set_cell(x,y,'end')
        else:
            self.clicked(x, y)

    def populate_grid(self):
        for y in range(0, self.height, self.grid_increment):
            self.canvas.create_line(y, 0, y, self.width)
            self.canvas.create_line(0, y, self.height, y)

            for x in range(0, self.width, self.grid_increment):
                cell = self.canvas.create_rectangle(x, y, x+self.grid_increment, y+self.grid_increment, fill='white')
                cell_status = {'cell':cell, 'filled':False, 'start':False, 'end':False}
                self.cells[x,y] = cell_status
                self.canvas.tag_bind(cell,
                                     "<Button-1>",
                                     lambda event, x=x, y=y: self.cell_formator(x,y)
                                     )

        submit_button = tk.Button(self, text ="Submit", command = self.submit)
        submit_button.pack()
        start_button = tk.Button(self, text ="Set Start Point", command = self.select_start)
        start_button.pack()
        end_button = tk.Button(self, text ="Set End Point", command = self.select_end)
        end_button.pack()
        submit_button.wait_variable(self.submitted)

    def clicked(self, x, y):
        cell = self.cells[x,y]['cell']
        if not self.cells[x,y]['start']:
            self.set_color(cell, 'black','white')
            self.cells[x,y]['filled'] = True

    def path_finder(self):
        self.start = self.start[::-1] #reversing coord direction 
        self.end = self.end[::-1] #reversing coord direction
        startNode = Node(None,self.start)
        endNode = Node(None,self.end)

        open_list = []
        closed_list = []
        maze_upper_limit = len(self.maze) - 1 #assumes nxn maze

        open_list.append(startNode)

        while len(open_list) > 0:
            # Sort to find node with lowest f score
            open_list.sort(
                key=lambda x:x.f)
            # Set as current node and remove from open, append to closed list
            currentNode = open_list[0]
            open_list.pop(0)
            closed_list.append(currentNode)

            if currentNode == endNode:
                print('Path Found')
                path = []
                current = currentNode
                while current is not None:
                    path.append(current.position)
                    current = current.parent_node
                path = path[::-1] # Return reversed path
                x_path = [x[1] for x in path]
                y_path = [y[0] for y in path]
                return x_path, y_path

            children = [(-1,-1),(-1,0),(-1,1),
                        (0,-1),        (0,1),
                        (1,-1),(1,0),(1,1)]

            for child in children:
                position = tuple(np.subtract(currentNode.position, child))
                childNode = Node(currentNode,
                                 position)
                # Check if within boundaries of maze
                if childNode.position[0] < 0 or childNode.position[0] > maze_upper_limit or childNode.position[1] < 0 or childNode.position[1] > maze_upper_limit:
                    continue
                # check if child if a wall
                if self.maze[childNode.position[0]][childNode.position[1]] == 1:
                    continue

                if childNode in closed_list:
                    continue

                childNode.g = currentNode.g + 1
                # childNode.g = currentNode.g + tuple(np.subtract(endNode.position, childNode.position))[0]**2 + tuple(np.subtract(endNode.position, childNode.position))[1]**2
                child_end_components = tuple(np.subtract(endNode.position, childNode.position))
                childNode.h = child_end_components[0]**2 + child_end_components[1]**2
                childNode.f = childNode.g + childNode.h

                for node in open_list:
                    if childNode == node and childNode.g > node.g:
                    # if childNode == node:
                        continue

                open_list.append(childNode)

    def plot_maze(self):
        cmap = colors.ListedColormap(['white', 'blue'])
        bounds = [0,0.5,1]
        norm = colors.BoundaryNorm(bounds, cmap.N)

        fig, ax = plt.subplots()
        ax.imshow(self.maze, cmap=cmap, norm=norm)

        # draw gridlines
        ax.grid(which='major', axis='both', linestyle='-', color='k', linewidth=2)
        ax.set_xticks(np.arange(-.5, self.grid_line_count, 1))
        ax.set_yticks(np.arange(-.5, self.grid_line_count, 1))
        ax.axes.xaxis.set_ticklabels([])
        ax.axes.yaxis.set_ticklabels([])
        plt.scatter([self.start[1],self.end[1]],[self.start[0],self.end[0]], color='r')
        plt.plot(self.x_path, self.y_path)
        plt.show()

    def main(self):
        self.populate_grid()
        print(f'start:{self.start}, end:{self.end}')
        self.x_path, self.y_path = self.path_finder()
        self.plot_maze()
        self.exit_canvas()

if __name__ =='__main__':
    solver = AStar()
    solver.main()












